$(document).on("ready", function(){

    //  DOM element declarations
    var dom = $(this);
    var emailForm = dom.find("form#email");
    var emailAlreadyEntered = false;
    var menuArea = dom.find("#headerBottom");
    var menuElements = dom.find("ul li");

    //  Constant definitions
    const DELAY_TIMING = 200;

    if(localStorage.getItem("emailEntered")){
        emailAlreadyEntered = true;
        domReshuffleForAcceptedEmail();
    }

    //  Events to be handled in the DOM.
    menuArea.hover(
        onHover().enter,    //  handles 'mouseenter' event
        onHover().leave     //  handles 'mouseleave' event
    );

    emailForm.submit(function(evt){

        evt.preventDefault();

        if(!emailAlreadyEntered){
            onSubmit.bind(this)(evt);
        }

    });

    function onHover(){
        return {
            enter : function (){

                //  Check to ensure that no buggy behaviour occurs
                //  in between both events being fired.
                if(!menuArea.hasClass("increaseWidth")){
                    menuArea.addClass("increaseWidth");
                    setTimeout(function(){
                        menuElements.addClass("show");
                    }, DELAY_TIMING);
                }

            },
            leave : function (){

                //  Check to ensure that no buggy behaviour occurs
                //  in between both events being fired.
                if(menuArea.hasClass("increaseWidth")){
                    setTimeout(function(){
                        menuElements.removeClass("show");
                        setTimeout(function(){
                            menuArea.removeClass("increaseWidth");
                        }, DELAY_TIMING);
                    }, DELAY_TIMING);
                }

            }
        }
    }

    function onSubmit(evt){

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="_token"]').attr('content')
            }
        });

        var data = $(this).serializeArray();
        var url = $(this).attr("action");

        var jsonifiedData = {};
        data.map(function(item) { jsonifiedData[item.name] = item.value; });

        if(validateEmail(jsonifiedData.email)){

            var postRequest = $.ajax({
                url: url,
                type: "POST",
                dataType: "json",
                data: jsonifiedData
            });

            postRequest.done(function(){

                localStorage.setItem("emailEntered", true);
                localStorage.setItem("email", jsonifiedData.email);

                domReshuffleForAcceptedEmail.bind(this)();

            }.bind(this));

        }

    }

    /**
    * Acquired from StackOverflow - (link : http://blah.stackoverflow.com)
    *
    * @param email string param describing
    * */
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    /**
    * Handles the showing of an appropriate footer region, depending on whether
    * a user has already entered their email address for subscription.
    * */
    function domReshuffleForAcceptedEmail(){
        var email = localStorage.getItem("email");
        emailForm.replaceWith("<img src='assets/images/tick.png' class='emailEntered'><p class='content'>Thank you, <strong>" + email + "</strong>. </p>");
    }

});