<script type="text/javascript" src="{{{ URL::asset("/assets/js/jquery.min.js") }}}"></script>
@foreach($include as $file)
    <script type="text/javascript" src="{{{ URL::asset("/assets/js/$file") }}}"></script>
@endforeach
