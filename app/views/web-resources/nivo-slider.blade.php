<link rel="stylesheet" href="{{{ URL::asset("assets/nivo/nivo-slider.css") }}}" type="text/css" />
<link rel="stylesheet" href="{{{ URL::asset("assets/nivo/default.css") }}}" type="text/css" media="screen">
<script src="{{{ URL::asset("assets/nivo/jquery.nivo.slider.pack.js") }}}" type="text/javascript"></script>

<script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
            controlNav: false,
            controlNavThumbs: false,
            pauseTime: 5000
        });
    });
</script>
