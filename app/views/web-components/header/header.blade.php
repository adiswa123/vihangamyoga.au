<header>
    <div id="headerBottom">
        <div id="navigationArea">
            <div class="innerArea">
                <nav>

                    <?php
                        //  PHP snippet to setup the array required by the link
                        //  generator
                        $links = array(
                            ["/home", "Home", "assets/images/home.png"],
                            ["/benefits", "Benefits", "assets/images/tick.png"],
                            ["/vihangam-yoga", "Vihangam Yoga", "assets/images/vy.png"],
                            ["/social-work", "Social Work", "assets/images/social.png"],
                            ["https://www.youtube.com/user/vihangamyoga", "Media", "assets/images/media.png"],
                            ["/contact-us", "Contact Us", "assets/images/contact.png"]
                        );
                    ?>

                    @include("web-components.header.link-generator", array("links" => $links))

                </nav>
            </div>
        </div>
    </div>
</header>