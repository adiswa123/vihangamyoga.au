<?php
    define("HREF_POS", 0);
    define("TEXT_POS", 1);
    define("IMG_POS", 2);
?>

<ul>
    @foreach($links as $link)
        <li><a href="{{ $link[HREF_POS] }}" class="icon"><img src="{{ Image::url($link[IMG_POS]) }}"></a>{{ HTML::link($link[HREF_POS],$link[TEXT_POS], array("class" => "link")) }}</li>
    @endforeach

</ul>
