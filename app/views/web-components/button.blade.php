<div class="button {{ $classes or "" }}">
    <a class="text" href="{{ $link }}">{{  $text }}</a>
</div>