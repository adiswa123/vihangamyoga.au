<div id="footer">
    <div class="innerArea verticalCentre">
        <div class="width-half text-left left">
            (C)Australian Institute of Vihangam Yoga. All Rights Reserved 2014
        </div>
        <div class="width-half text-right right">
            <a href="#">Contact Us</a>
            |
            <a href="#">Legal</a>
            |
            <a href="#">Privacy</a>
        </div>
    </div>
</div>