@extends("layouts.page")

@section("content")

    <div class="innerArea">

        <p class="title">Benefits</p>
        <p class="subtitle">Understand the pros of engaging in Yogic practices.</p>

        <div class="contentAreas">
            <div class="contentArea">
                <p class="title">
                    Physical
                </p>
                <p class="content">

                    When functioning optimally on a physical level, famous sportsmen and sportswomen speak to great
                    lengths of having 'found their feet' and being <strong>'in the zone'</strong>. They speak not of technique, their opponent, or the crowd - all that is before them is a situation,
                    which they react to with all the skills they have refined over the course of time.

                    <br><br>

                    This experience, of being completely tuned into the current moment is something which all of us have
                    experienced - <strong>to become completely 'lost' or 'absorbed' in something</strong>. As much as this
                    is an intrinsic aftereffect of inner calmness and focus, it extends into the physical realm, for
                    calmness in mind has a direct correspondence to bodily balance.

                    <br><br>

                    <img src="assets/images/benefits_physical.jpg">

                    <br><br>

                    This is because, the primary focus of Yoga is to teach the art of self-control and balance. As such, the physical benefits
                    are immediate and vast. In short, it affords <strong>optimisation</strong> of one's physical state.
                    Amongst many physical benefits, the most salient ones, noticeable soon after the adoption of a method
                    of yoga and/or meditation are:

                    <ul class="benefitList">
                        <li>
                            <strong>Faster and better physical recovery:</strong>
                            Meditation improves the quality of your sleep.
                            When we go to sleep, often we find ourselves caught inside webs of thought, which we untangle
                            whilst starting at the ceiling for hours on end. That, or we fall asleep, and have troubling
                            or confusing dreams. By calming these currents of thought, we allow our body the chance to truly
                            relax, heal, and prepare for the next day.
                        </li>
                        <li>
                            <strong>Improves sensory perception:</strong>
                            In the Buddhist tradition, it is a known fact that after the process of
                            yoga and meditation, one's sensory perception is heightened and refined to a level unlike
                            ever before. From small increments such as sharper eyesight, meditation enriches your
                            interaction with the world.
                        </li>
                        <li>
                            <strong>Balances bodily functions:</strong>
                            Meditation increases energy levels, and decreases negative bodily effects
                            (such as high blood pressure). As much as meditation has a sobering effect on one's body,
                            it also allows one to draw out the energy they require in any given situation. In short,
                            through a greater sense of focus, it allows maximum output to be achieved both consciously
                            (moving, touching) and unconsciously (breathing, healing,
                            inner body functions). Examples of this include lower blood pressure.
                        </li>
                        <li>
                            <strong>Strengthens internal systems:</strong>
                            As touched upon above, the combined effect of greater healing, and energy levels is felt
                            both externally by us, and within our internal systems. Our nervous and immune systems are
                            instantly refined.
                        </li>
                    </ul>

                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Mental
                </p>
                <p class="content">
                    It is within our mental worlds that we truly exist. This is from where all our desires, thoughts
                    and actions originate, and truly, where we spend most if not all of our lives.

                    <br><br>

                    Although the exact number differs, it is said and has been researched, that humans have between
                    60 000 and 80 000 thoughts a day. The fact of the matter is, if we observe our own thoughts for
                    ourselves, many of these are wasted thoughts. What do I mean by wasted? Well, when you are focusing
                    on thing/object/action A, then is it logical to think about action B, C, D and so on so forth?

                    <br><br>

                    <img src="assets/images/benefits_mental.jpg">

                    <br><br>

                    This is where meditation comes in. The words 'calmness' and 'balance' and 'self-control', are often
                    used when talking about the mental benefits which meditation ushers. Ultimately, these are all the
                    results of what meditation truly provides a practitioner - the ability to give their undivided
                    attention and focus to what is before them. In specific:

                <ul class="benefitList">
                    <li>
                        <strong>Removes stress and clears the mind:</strong>
                        Different to the modern fascination with stress management techniques, meditation prides itself
                        as being a practice through which an individual completely removes themselves from the concept
                        and feeling of stress. By calming the turbulent currents of the mind, one is able to truly
                        gauge their potential to the fullest, allowing to exercise their creativity, intelligence and
                        overall mental abilities in an enhanced way unlike ever before.
                    </li>
                    <li>
                        <strong>Improves performance:</strong>
                        Whether it be a person in their youth, undergoing the challenges of school and/or university, or
                        an individual within/beyond the realms of the professional world, the calming effect of meditation,
                        and the way in which it allows us to remove ourselves from everything but the task at hand leads
                        to great improvements in performance.
                    </li>
                    <li>
                        <strong>Ushers self-control:</strong>
                        Through the unshackling of one's mental state from the turbulent nature of the mind, meditation
                        allows individuals to truly gain control of their every desire, thought and action. Be it
                        through the balancing of emotions such as anger and sadness, or the ability to free one's self
                        from habits and addictions, the art of self-control becomes a living and breathing part of a
                        practitioner's life.
                    </li>
                </ul>
                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Spiritual
                </p>
                <p class="content">

                    On a spiritual level, progress is defined and characterised by the extent to which an individual
                    practitioner is able to achieve balance within themselves and with the world. At the fundamental level,
                    this balance can only be achieved through an increased sense of awareness of both one's self,
                    and the world around them.

                    <br><br>

                    As cited in many Bhajans and verses of old, it is only through the removal of one's self from the
                    mind that this spiritual journey begins - <strong>spanning from an awareness of self, to an awareness
                    of the universe</strong>.

                    <br><br>

                    <img src="assets/images/benefits_spiritual.jpg">

                    <br><br>

                    Meditation facilitates this process of self-upliftment. It is in fact only through the practice of
                    meditation that one may enter their inner world, and learn the wonders of this universe and beyond.
                    In many ways, spiritual progress and direction <strong>provides a compass through which one may navigate
                    the difficulties of the world and life</strong>, whilst remaining bounded in a sense of calmness and
                    immovable confidence.

                    <br><br>

                    Rather than experiencing a sense of being lost amongst the infinite torrents of life and the world,
                    you will know where you are going, and be able to answer the bigger questions such as
                    <strong>'Who am I?'</strong> and <strong>'What is it I am here to do?'</strong>.

                </p>
            </div>
        </div>
    </div>
@stop