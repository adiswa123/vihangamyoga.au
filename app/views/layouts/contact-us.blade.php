@extends("layouts.page")

@section("content")

    <div class="innerArea">

        <p class="title">Contact Us</p>
        <p class="subtitle">Get in touch!</p>

        <div class="contentAreas">
            <div class="contentArea">
                <p class="content center">

                    <strong>We are based at:</strong>

                    <br>

                    4 Martens Circuit, Kellyville - 2155 NSW<br>

                    Home: (02) 8824 4423 <br>
                    Mobile: 0417 239 433

                    <br><br>

                    Or, drop us an email at <strong>spivyaustralia@gmail.com</strong>!

                </p>
            </div>
        </div>
    </div>

@stop