@extends("layouts.page")

@section("content")

    <div class="innerArea">

        <p class="title">Vihangam Yoga</p>
        <p class="subtitle">The Ancientmost Science of Spirituality</p>

        <div class="contentAreas">
            <div class="contentArea">
                <p class="title">
                    Beginnings
                </p>
                <p class="content">

                    A common question when it comes to any meditative or yogic practice is, 'Where did it start?'.
                    This often arises from one's desire to understand the motivations behind any given practice - is
                    it to help me purely on a physical level? Mentally? Or is there more to it?

                    <br><br>

                    The world has always been blessed with many wondrous souls, who impart knowledge of many different
                    forms. In Vihangam Yoga, we divide knowledge into being of two logical types - theoretical knowledge,
                    and practical knowledge.

                    <br><br>

                    Vihangam Yoga started with the journey of its founder, Sadafaldeo Maharaj, and began it's spread
                    with the enlightened Shri Sadguru Sadafaldeoji Maharaj from 1947. In his own words, Sadafaldeo
                    explained to his disciples, that this journey, which took him from his home village to all parts of
                    India sourced from his one curiosity - to attain and become one with the universe.

                    <br><br>

                    It is noted in many ancient scripts of humankind, that before the advent of any religious body or
                    doctrine, the way in which humans learnt of the universe was through looking within - for everything
                    we see outside is a manifestation of what lies within. This science, of uncovering the secrets and
                    inner workings of the universe and beyond was known to be a body of knowledge accessible to the sages
                    of old, who rather than reading texts and books (acquiring theoretical knowledge), learnt and
                    practised a powerful technique of meditation which revealed the myriads of the universe to them. It is
                    this civilisation and group of individuals from which arose the ancientmost texts known to humanity,
                    the Vedas.

                    <br><br>

                    As such, upon embarking his journey, Sadafaldeo did not want to read others experiences or accounts
                    in achieving this spiritual laurel - he spent his spiritual journey searching for ways in which
                    he could experience and preach this science himself.

                    <br><br>

                    Once he acquired this knowledge, the now Sadguru, the harbinger of the divine knowledge in the modern
                    era, took it upon himself to share it with humanity, and famously proclaimed:

                    <p class="doha">
                        Atbuth maarag yog vihangam, mein tumko bathlaunga,<br>
                        Yadhi vidhiwat tum sadhan kariho, amarlok pahuchaunga<br>
                    </p>

                    <br>

                    <p class="explanation">
                        Wonders is the way of Vihangam Yoga. If thou practiseth as prescribed,<br>
                        verily will I leed thee to the land of immortality.
                    </p>

                    <br>

                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Motivations and Differences
                </p>
                <p class="content">

                    In the spiritual world, knowledge can be imparted in two ways - through theoretical and practical means.
                    To say this differently, we may either understand, or experience any given physical, mental, or
                    spiritual impact of meditation and yoga.

                    <br><br>

                    The primary motivation behind and difference between Vihangam Yoga and all other practices of the
                    modern era is this - Vihangam Yoga prides itself and aims to provide the practical experience of
                    meditation to one and all.

                    <br><br>

                    You will find whatever you are searching for in this world within Vihangam Yoga, for rather than being
                    a purely physical or mental practice, the roots of Vihangam Yoga have sprung from spiritual
                    curiosity, which inherently goes beyond all aspects of the physical and mental worlds.

                    <br><br>

                    A good example of this is that often we find that within our lives that as time goes on, our thoughts
                    shift from focusing not on our desires, thoughts or actions, but moreso on aspects of knowledge -
                    'Why am I here?', 'What am I to do with my life?'. It is within these moments, when we wonder, and ponder
                    that truly stay with us and shape us throughout lives. These internal curiosities are not of a physical or mental
                    nature, but rather, of a spiritual nature.

                    <br><br>

                    They are to do with aspects of living and existing - and as you can imagine, once these concerns are
                    satisfyingly answered, the confidence and assurance one acquires from them filters into all aspects
                    of our lives, as we are truly able to appreciate both everything within us, and the world around us.

                    <br><br>

                    But where does this journey begin? And how is it facilitated?

                    <br><br>

                    As explained in many spiritual traditions of old and new, one must first remove themselves from
                    the shackles of the mind. It is due to the mind, and it's ever-desiring and ever-changing
                    nature, that we find ourselves trapped in this confusing world. Imagine if you were to shake your head furiously whilst in the ocean - would you be able to see
                    clearly ahead of you? You would only be able to capture momentary glimpses of what truly lies ahead,
                    and you would find yourself guessing which way to go.

                    <br><br>

                    Are our lives not but a real version of this metaphor? We live so very fast, for the world itself
                    requires this of us. We are always balancing multiple things on the fly, and unintentionally find
                    ourselves burning out all our energies. Ideally, what should happen? We should be able to see clearly
                    ahead, and be distant from all distractions. Vihangam Yoga does exactly this.

                    <br><br>

                    It is not just any meditative practice - Vihangam Yoga blesses one with the
                    gift of clarity and art of self-control, so that you may truly take control of your life. No
                    matter how many waves comes your way, or how many obstacles present themselves before you,
                    you will remain calm, composed, and in control.

                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Vihangam Yoga and Life
                </p>
                <p class="content">

                    What does Vihangam Yoga offer in terms of life benefits?

                    <br><br>

                    One of the most prominent aftereffects of acquiring a sense of balance and calmness within one's self,
                    is the development of an immovable sense of self-confidence and self-belief.

                    <br><br>

                    As explained by VY, through the sense of focus and sharpness that meditation provides, one is assured
                    that no matter what obstacles are encountered, one cannot be perturbed from the path towards
                    achieving their goals in life. This distinguishable strength of self-belief is considered to be
                    beyond and much stronger than any level of physical, mental and intellectual strength and capacity.

                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Swarved - Our Holy Scripture
                </p>
                <p class="content">
                    What is the Swarved?

                    <br><br>

                    The Swarved is our salient holy-scripture. It was written by our founder, Sadguru Sadafaldeo Maharaj
                    in his enlightened state after 17 years of meditation.
                    Swarved is the means of spiritual growth of mankind, the spiritual welfare of mankind.
                    It is the path by which the soul can realize itself, the path by which that Supreme Bliss can be obtained.

                    <br><br>

                    Some dohas from the Swarveda
                    <br>
                    <p class="doha">
                        Satya asatya sei alaga hai, so para satya svarupa
                        <br>
                        Akatha alaukika tatva hai, aja anadi vara rupa
                    </p>
                    <p class="explanation">
                        The Supreme Soul pervades the eternal entities and exists in three quarters of his zone in purely
                        bliss form. Speach and senses can't describe him. The indefinable Supreme Being is separate from
                        material objects and exists in a pure, conscious and effulgent form. There is no object that he can
                        be associated with. He is all pervading and beyond time and space. He is the master and creator of
                        all that have names and forms in the universe.
                    </p>
                </p>
            </div>
        </div>
    </div>
@stop

{{--Human Life--}}
    {{--Purpose of Life--}}
    {{--Flow of Life--}}
    {{--Optimum Use of Time in Life--}}
    {{--Goal of Life--}}
    {{--Preciousness of Human Body--}}
    {{--Preciousness of Time--}}
    {{--God-Realisation as a Goal--}}
    {{--Industriousness and Innovativeness--}}
    {{--Self-confidence--}}
    {{--Daily Routine--}}
    {{--Morning Time (Brahm Muhurth)--}}
    {{--Hope is Life--}}
{{--Religion--}}
    {{--Meaning of Religion--}}
    {{--Belief--}}
    {{--What is a seeker--}}
    {{--Self-realisation--}}
{{--Vihangam Maarg--}}
    {{--Yog, Siddhi Bibuthi, Detachment--}}
    {{--Pranayama--}}
    {{--Path to God--}}
    {{--Science of Conciousness--}}
    {{--Five Stage of Yog--}}
    {{--What is Love--}}
{{--Sadguru--}}
{{--Swarveda--}}
{{--Shanti--}}