@extends("layouts.master", array("landing" => true))

@section("head")
    @include("web-includes.css", array( "include" => array("styles_main.css", "helpers.css", "styles_reset.css")))
@stop

@section("content")
<div id ="landingWrapper">
    <div id="wrapper_title">
        <img src="assets/images/img_logo.png" id="logo">
        <p class="title">
            Are you stressed?
            <br>
            Not reaching your potential?
            <br> <br>
            Do you feel as if, there is a greater level of achievement and life waiting for you?
            <br><br>
            At Vihangam Yoga, we all know of that feeling - and we are here to tell you, that the door to your transformation lies within.
        </p>
    </div>
    @include("web-components.button", array("text" => "Take Me There!", "link" => "/home", "classes" => "landing"))
</div>

@stop
