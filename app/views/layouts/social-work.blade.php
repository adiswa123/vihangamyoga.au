@extends("layouts.page")

@section("content")

    <div class="innerArea">

        <p class="title">Social Work</p>
        <p class="subtitle">Progressing Humanity</p>

        <div class="contentAreas">
            <div class="contentArea">
                <p class="title">
                    SSDVYS - Sadguru Sadafaldeo Vihangam Yoga Sansthan
                </p>
                <p class="content">

                    A UN-affiliated organisation, SSDVYS is a body which aims at the overall upliftment of humankind
                    through the practice of meditation. In doing so, SSDVYS offers different avenues of support to the needy,
                    ranging from health based initiatives, to the development of professional skillsets.

                    <br><br>

                    As an organisation, its primary aim aligns itself with the ideologies brought forward by
                    Sadguru Sadafaldeo - to establish and maintain universal peace and well-being amongst all
                    individuals.

                    <br><br>

                    For more information, visit <a href="http://un.ssdvy.org/" class="custom">un.ssdvys.org</a>.

                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Vihangam Life Workshops
                </p>
                <p class="content">

                    Through the simple addition of a method of meditation to your life, Vihangam Yoga guarantees
                    transformation in all aspects of your living. Whether it be the way you experience all the things
                    you do day-to-day, to the development of an ever deeper appreciation and love for the world,
                    it truly touches every aspect of your life, and enhances it.

                    <br><br>

                    Vihangam Life is a workshop centred around explaining and enhancing these changes. It achieves this
                    through the teaching of additional practices which further your progress along this path. In specific,
                    it teaches the art of Asanas and Pranayamas (known commonly now as 'Yoga'), the nature and advantages
                    of particular diets adopted by the sages of old, Ayurvedic medical practices and tips, and most
                    importantly, provides the opportunity to interact with the knowledge of Vihangam Yoga through both the
                    clearing doubts and discussing different aspects with experienced practitioners and explanatory
                    recitations of the Swarveda.

                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Educational Services and Awareness Programs
                </p>
                <p class="content">

                    In order to truly provide a way for the needy to take control of their lives, they require access to
                    socially valued resources, of which education is most primary. Through the medium
                    of bodies such as SSDVYS and Ashram-centric activities, those who cannot afford an education are
                    transitioned into students free of charge, receiving a Vedic education and upbringing at one of our
                    schools in Ballia or Allahabad.

                    <br><br>

                    In addition to this, it is often the case that those who are unable to forge their own path through
                    life result to addictive tendencies. This means that even when provided the opportunity, they find
                    it difficult to remove themselves from their old mannerisms. In order to counter this, we run
                    awareness programs all over the globe, targeted at educating both young and old in regards to what
                    a healthy and complete lifestyle is truly about, whilst reaching out to those who require the help
                    they need to unshackle themselves from addictions.

                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Relief Work and Employment
                </p>
                <p class="content">

                    When natural disasters strike, members of the Vihangam Yoga community ensure all aid required
                    is provided to those affected. In addition to this, on a regular basis, essentials such as food
                    and clothing are distributed to the needy through Vihangam Yoga ashrams.

                    <br><br>

                    The issue of employment both to those in their youth, and old-aged, is also something tackled by the
                    VY community. Through vocational education offerings, appropriate pathways and support is provided
                    to these individuals, allowing them to take hold of the future they desire.

                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Cultural Services
                </p>
                <p class="content">

                    Amongst all globally placed VY communities, the process of Vedic Hawans, a cultural service aimed at
                    bettering the environment is done with regularity. Hawans are ancient and scientific methods of
                    atmospheric detoxifiation, which aid in bringing rain, removing foul odors, insects and bacteria, and
                    curing chronic diseases.

                    <br><br>

                    As we are also living and breathing parts of the environment and worldly ecosystem, Hawans provide
                    advantages to both us, and all the living beings which share this world with us.

                </p>
            </div>

        </div>
    </div>

@stop