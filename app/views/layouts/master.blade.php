<html>

    <head>
        <meta charset="UTF-8">
        <base href="{{ Config::get("app.url") }}">
        <title>The South Pacific Institute of Vihangam Yoga</title>
        @include("web-includes.fonts")
        @yield("head")
    </head>

    @if(!isset($landing))
        @include("web-components.header.header")
    @endif

    <body>
        @yield("content")
    </body>

</html>
