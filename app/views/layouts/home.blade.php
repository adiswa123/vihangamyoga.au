@extends("layouts.master")

@section("head")
	@include("web-includes.css", array( "include" => array("styles_main.css", "helpers.css", "styles_reset.css", "fullcalendar.min.css")))
	@include("web-includes.js", array( "include" => array("imagesLoaded.min.js", "main.js", "moment.min.js", "fullcalendar.min.js", "gcal.js"))) {{--includes jquery by default--}}
	@include("web-resources.nivo-slider")
	<meta name="_token" content="{{ csrf_token() }}" />
@stop

@section("content")

	<script type='text/javascript'>

		$(document).ready(function() {
			$('#calendar').fullCalendar({
				googleCalendarApiKey: 'AIzaSyDpKT3vpercXYzPFAgPZqNArlDNU7Og78E',
				events: {
					googleCalendarId: 'eh1pa0vfc05p22p4vq4tric9b8@group.calendar.google.com'
				},
				fixedWeekCount: false,
				height: 375,
				header: {
					left:   '',
					center: 'title today prev,next',
					right:  ''
				},
				titleFormat: "MMMM YYYY",
				buttonText: {
					today:    'Today',
					month:    'month',
					week:     'week',
					day:      'day'
				}
			});
		});

	</script>

	<div class="flash-component">
		<div class="innerArea">
			<div id="wrapper_all">
				<div id="wrapper_title">
					<img src="{{ Image::url("/assets/images/img_logo.png") }}" id="logo">
					<p class="title">The South Pacific Institute <br> of Vihangam Yoga</p>
				</div>
				<div class="wrapper_flash theme-default">
					<div id="slider">
						<img src="{{ Image::url("/assets/images/img_flash1.png") }}">
						<img src="{{ Image::url("/assets/images/img_flash2.png") }}">
						<img src="{{ Image::url("/assets/images/img_flash3.png") }}">
						<img src="{{ Image::url("/assets/images/img_flash4.png") }}">
						<img src="{{ Image::url("/assets/images/img_flash5.png") }}">
						<img src="{{ Image::url("/assets/images/img_flash6.png") }}">
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="introduction-component">
		<div class="innerArea">
			<p class="title">Vihangam Yoga - your entrance to the world of spirituality.</p>
			<p class="content">
				Yoga or true 'Yog', is the practice of meditation. At SPIVY, this is what we teach - the most powerful
				meditation technique known to humankind, and available to one and all.
			</p>
			{{-- TODO Add testimonials in, once available--}}
			{{--<div class="testimonials">--}}
				{{--<div class="testimonial">--}}
					{{--<img src="{{ Image::url("/assets/images/testimonial1.png") }}">--}}
					{{--<p class="testimonialText">It's changed a lot about me - I feel very much balanced and in control now.--}}
						{{--<br>--}}
						{{--<span>- Abhinav Kishore, Student at University of New South Wales</span>--}}
					{{--</p>--}}
				{{--</div>--}}
			{{--</div>--}}
		</div>
	</div>

	<div class="content-component">
		<div class="innerArea">
			<div class="text-container">
				<p class="title">Why Yoga?</p>
				<p class="content">
					Our thoughts and actions emerge from our internal worlds, and consciousness.
					The singlemost beneficial aspect of meditation relates to this very fact - it provides a door to your inner
					self, facilitating self-reflective growth in all areas of life.
				</p>
			</div>
			<div class="content-container">
				<p class="subtitle">Physical</p>
				<img src="{{ Image::url("/assets/images/physical.png") }}">
				<p class="content">
					Meditation calms the body, through calming the mind. As such,
					it adds a layer of ease and rhythm to our daily interactions, allowing us to peak physically in our
					daily lives.
				</p>
			</div>
			<div class="content-container">
				<p class="subtitle">Mental</p>
				<img src="{{ Image::url("/assets/images/mental.png") }}">
				<p class="content">
					Through practice, meditation brings mental peace and satisfaction.
					This manifests itself in many ways, including a removal of yourself from all stress.
				</p>
			</div>
			<div class="content-container">
				<p class="subtitle">Spiritual</p>
				<img src="{{ Image::url("/assets/images/spiritual.png") }}">
				<p class="content">
					It is only through a controlled mind that one may progress spiritually. Meditation thus
					facilitates one's spiritual journey - the journey to discover what lies within.
				</p>
			</div>
		</div>
	</div>

	<div class="information-component">
		<div class="innerArea">
			<div class="left-content-wrapper">
				<div class="smallContentContainer sukrit clean">
					<p class="title">Sukrit Wellness Magazine</p>
					<img src="{{ Image::url("/assets/images/sukrit.jpg") }}">
					<p class="content">
						Sukrit is a spiritual lifestyle and wellness magazine aimed at exploring the
						ancient knowledge of Indian seers and sages in the context of modern day life.
						Access it through
						<a href="https://play.google.com/store/apps/details?id=com.magzter.sukrit">	Android</a>,
						<a href="https://itunes.apple.com/us/app/sukrit/id912786781?mt=8">iOS</a>,
						or the <a href="http://www.magzter.com/IN/Sukrit/Sukrit/Health">Web</a> now!
					</p>
				</div>
				<div class="smallContentContainer upcomingEvents clean">
					<p class="title">Events for</p>
					<div id="calendar"></div>
					<p class="content">View Official Calendar <a href="https://www.google.com/calendar/embed?src=eh1pa0vfc05p22p4vq4tric9b8%40group.calendar.google.com&ctz=Australia/Sydney" class="custom" target="_blank">here!</a></p>
				</div>
			</div>
			<div class="largeContentContainer">
				<p class="title edited">VY in Australia</p>
				<div class="containerSectionsWrapper">
					<div class="containerSection">
						<div class="bottom australia">
							<img src="{{ Image::url("/assets/images/australiaprogram.jpg") }}">
							<img src="{{ Image::url("/assets/images/australiaprogram2.jpg") }}">
							<img src="{{ Image::url("/assets/images/australiaprogram3.jpg") }}" class="last">
							<p class="content">
								Formally established in 2012, SPIVY has been active in Australia since 1987. From
								the younger generations to professionals, and the retired, SPIVY has touched the lives of
								hundreds of people around Australia. Regular sessions are conducted by our volunteers,
								aimed both at providing a clear understanding of what yoga is, but more importantly, teaching
								HOW to meditate.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="largeContentContainer">
				<p class="title edited">Recent Articles</p>
				<div class="containerSectionsWrapper">
					@foreach ($articles as $article)
					<div class="containerSection">
						<div class="top">
							<img src="{{ Image::url("/assets/images/articles". $article->id .".jpg") }}">
							<p class="title"> {{ $article->heading }}</p>
							<p class="content">{{ $article->author }}</p>
						</div>
						<div class="bottom">
							<p class="content">
								{{ $article->preview }}
								<a href="articles/{{ $article->id }}">&rarr;</a>
							</p>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<div class="updated-component">
		<div class="innerArea">
			<p class="title">Stay Updated!</p>
			<p class="content">
				Provide your email below and we'll make sure to keep you posted on upcoming events and articles/publications!
			</p>
			{{ Form::open(array("url" => URL::to("/email", null, null), "id" => "email")) }}
				{{ Form::text("email", null, array("placeholder" => "Please enter your email here: example@mail.com")) }}
				<input type="submit" style="position: absolute; left: -9999px"/>
			{{ Form::close() }}
		</div>
	</div>
@stop
