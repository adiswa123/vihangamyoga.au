@extends("layouts.master")

@section("head")
    @include("web-includes.css", array( "include" => array("styles_main.css","styles_pages.css", "helpers.css", "styles_reset.css")))
    @include("web-includes.js", array( "include" => array("main.js"))) {{--includes jquery by default--}}
    @include("web-resources.nivo-slider")
@stop

