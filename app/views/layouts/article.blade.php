@extends("layouts.page")

@section("content")

    <div class="innerArea">

        <div class="contentAreas">

            @foreach($articles as $article)
            <div class="contentArea">
                <img src="{{ Image::url("/assets/images/articles" . $article->id . ".jpg") }}" class="header">
                <p class="title"> {{ $article->heading }} </p>
                <p class="author"> By {{ $article->author }} </p>
                <p class="content">
                    @if($showAll)
                        {{ $article->preview }}
                        <br><br>
                        <a href="/articles/{{ $article->id }}">Read More &rightarrow;</a>
                    @else
                        {{ $article->data }}
                    @endif
                </p>
            </div>
            @endforeach

        </div>

    </div>

@stop