@extends("layouts.page")

@section("content")

    <div class="innerArea">

        <div class="contentAreas">
            <div class="contentArea">
                <p class="title">
                    Physical
                </p>
                <p class="content">

                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Fusce purus metus, vestibulum a accumsan eu, varius non diam.
                    Aenean iaculis semper tellus, nec aliquet sem.
                    Quisque mollis tortor ac justo condimentum ornare nec sed mauris.
                    Proin accumsan turpis eget placerat condimentum.

                    <br>

                    Mauris tempor vitae arcu et tincidunt.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.
                    Curae; Sed convallis finibus aliquam. Suspendisse tristique massa ante, eu aliquam nunc sollicitudin ut.
                    Phasellus malesuada at est sed imperdiet.
                    In dictum blandit velit eu maximus.
                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Mental
                </p>
                <p class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Fusce purus metus, vestibulum a accumsan eu, varius non diam.
                    Aenean iaculis semper tellus, nec aliquet sem.
                    Quisque mollis tortor ac justo condimentum ornare nec sed mauris.
                    Proin accumsan turpis eget placerat condimentum.

                    <br>

                    Mauris tempor vitae arcu et tincidunt.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.
                    Curae; Sed convallis finibus aliquam. Suspendisse tristique massa ante, eu aliquam nunc sollicitudin ut.
                    Phasellus malesuada at est sed imperdiet.
                    In dictum blandit velit eu maximus.
                </p>
            </div>

            <div class="contentArea">
                <p class="title">
                    Spiritual
                </p>
                <p class="content">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Fusce purus metus, vestibulum a accumsan eu, varius non diam.
                    Aenean iaculis semper tellus, nec aliquet sem.
                    Quisque mollis tortor ac justo condimentum ornare nec sed mauris.
                    Proin accumsan turpis eget placerat condimentum.

                    <br>

                    Mauris tempor vitae arcu et tincidunt.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia.
                    Curae; Sed convallis finibus aliquam. Suspendisse tristique massa ante, eu aliquam nunc sollicitudin ut.
                    Phasellus malesuada at est sed imperdiet.
                    In dictum blandit velit eu maximus.
                </p>
            </div>
        </div>

        <div class="additionalAreas">
            @include("web-helpers.quickLinks")
            @include("web-helpers.articles")
        </div>

    </div>

@stop