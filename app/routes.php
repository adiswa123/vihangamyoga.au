<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
		return View::make('layouts.landing');
});

Route::get("articles/{articleId}", function($articleId){
	return App::make("ActiveController")->getIndex("article", $articleId);
});

Route::get("/{pageUrl}", function($pageUrl)
{
		return App::make("ActiveController")->getIndex($pageUrl);
});

Route::post("/email", array('before'=>'csrf','uses' => "ActiveController@addEmail"));