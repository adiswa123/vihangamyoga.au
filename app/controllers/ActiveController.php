<?php

use Hugofirth\Mailchimp\Facades\MailchimpWrapper;

class ActiveController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Active Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'ActiveController@{pageUrl}');
    |
    */

    public function getIndex($pageUrl, $optionalId = null)
    {
        switch($pageUrl)
        {
            case "home":
                return ActiveController::showHome();
            case "benefits":
                return ActiveController::showBenefits();
            case "vihangam-yoga":
                return ActiveController::showVY();
            case "articles":
                return ActiveController::showArticles();
            case "article":
                return ActiveController::showArticle($optionalId);
            case "social-work":
                return ActiveController::showSocialWork();
            case "media":
                return ActiveController::showMedia();
            case "contact-us":
                return ActiveController::showContactUs();
            default:
                return ActiveController::showError();
        }
    }

    public function addEmail()
    {
        $email = Input::get("email");

        $userEmail = new VYEmail;
        $userEmail->email = $email;

        $userEmail->save();
        MailchimpWrapper::lists()->subscribe("b1e967988e", array('email'=>$email));
    }

    private function showHome()
    {
        $numArticles = 2;
        $articles = VYArticle::orderBy('dateAdded', 'desc')->take($numArticles)->get();

        return View::make("layouts.home")
            ->with("articles", $articles);
    }

    private function showBenefits()
    {
        return View::make("layouts.benefits");
    }

    private function showVY()
    {
        return View::make("layouts.vihangam-yoga");
    }

    private static function showArticles()
    {
        $articles = VYArticle::all();

        return View::make("layouts.article")
            ->with("articles", $articles)
            ->with("showAll", true);
    }

    private static function showArticle($articleId)
    {
        if($articleId < sizeof(VYArticle::all()) && is_numeric($articleId))
        {
            $articleObj = VYArticle::find($articleId);

            $articles = array();
            array_push($articles, $articleObj);

            return View::make("layouts.article")
                ->with("articles", $articles)
                ->with("showAll", false);
        }
        else
        {
            return ActiveController::showError();
        }

    }

    private static function showSocialWork()
    {
        return View::make("layouts.social-work");
    }

    private static function showMedia()
    {
        return View::make("layouts.media");
    }

    private static function showContactUs()
    {
        return View::make("layouts.contact-us");
    }

    private static function showError()
    {
        return View::make("layouts.error");
    }
}
