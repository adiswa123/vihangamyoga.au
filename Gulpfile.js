//  Get Gulp ready to run "aaaall" the things
var gulp = require("gulp");
var less = require("gulp-less");
var notify = require('gulp-notify');

var cssDir = "public/assets/css/*.less";
var targetDir = "public/assets/css/";

gulp.task("less", function(){
    gulp.src(cssDir)
        .pipe(less())
        .pipe(gulp.dest(targetDir))
        .pipe(notify('CSS done m8'))
});

gulp.task("default", function(){
    gulp.watch(cssDir, ["less"]);
});